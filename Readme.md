﻿# Untitled kata #
  
Resume of the kata  
  
### Set up ###
  
Branch this first commit, add the name of the kata to the branch name and update this file with the concrete kata details
  
Use a TDD aproach.  
Do a commit after each test, implementation, and refactor you do.   
 
Share the final result with whoever you need to  
:-)  
  
### Kata description ###
  
Update this description...   
Make sure you only test for correct inputs. there is no need to test for invalid inputs for this kata  
  
The rules are the following:  
  
* ...  
